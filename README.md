-----------------------------------------

VEGETA SURVIVOR
Version 1.5

A game by Alejandro Mendez

-----------------------------------------

CONTROLS

Arrows ------ Move

A ----------- Left

S ----------- Down

D ----------- Right

W ----------- Up

Space ------- Dash

Scroll Mouse- Change Ability

Click right - Shoot

----------------------------------------

DATA SHEET

Genere: Fantasy, Survivor

Ability of the game: Can shoot skills

Teme: Roguelike

Players: 1

-----------------------------------------

Start the game on the Menu screen because otherwise the bullets will go through the enemies and the GameManager will not initialize.
When you complete three rooms you will win. 
If you leave in the middle of the game, you wil lose the rewards earned.
The enemies drop three diferents items (Health, Coins and Ability). 

-----------------------------------------

The only improvement implemented compared to the last delivery has been the implementation of the dash.
In this project I found an error that I couldn't solve, which was OnParticleCollision (when the particles collide, they don't enter that method)
