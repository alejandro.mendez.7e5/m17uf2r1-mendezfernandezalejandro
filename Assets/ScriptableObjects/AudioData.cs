using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Audio Data", menuName = "Audio Data")]
public class AudioData : ScriptableObject
{
    [SerializeField] private AudioClip _audioDeathEnemy;
    [SerializeField] private AudioClip _audioDeathPlayer;
    [SerializeField] private AudioClip _audioTPlayer;
    [SerializeField] private AudioClip _audioPamPlayer;

    public AudioClip AudioDeathEnemy { get { return _audioDeathEnemy; } }
    public AudioClip AudioDeathPlayer { get { return _audioDeathPlayer; } }
    public AudioClip AudioTPlayer { get { return _audioTPlayer; } }
    public AudioClip AudioPamPlayer { get { return _audioPamPlayer; } }
}
