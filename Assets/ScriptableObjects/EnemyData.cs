using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Data", menuName = "Enemy Data")]
public class EnemyData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private float _health;
    [SerializeField] private int _speed;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private float _velocityBullet;
    public GameObject[] _loot = new GameObject[2];
    public GameObject _key;



    public string Name { get { return _name; } set { _name = value; } }
    public float Health { get { return _health; } set { _health = value; } }
    public int Speed { get { return _speed; } set { _speed = value; } }
    public GameObject Bullet { get { return _bullet; } set { _bullet = value; } }
    public float VelovityBullet { get { return _velocityBullet; } set { _velocityBullet = value; } }
    public GameObject Key { get { return _key; } set { _key = value; } }
}
