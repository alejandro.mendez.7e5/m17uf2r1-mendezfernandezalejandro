using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "New Hability Data", menuName = "Hability Data")]
public class HabilityData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private int _damage;
    [SerializeField] private float _time;
    [SerializeField] private int _cost;
    [SerializeField] private GameObject _ball;
    [SerializeField] private float _speed;
    [SerializeField] private Sprite _spriteBall;
    [SerializeField] private string _description;
    [SerializeField] private int _price;

    public string Name { get { return _name; } }
    public int Damage { get { return _damage; } }
    public float Time { get { return _time; } }
    public float Cost { get { return _cost; } }
    public GameObject Ball { get { return _ball; } }
    public float Speed { get { return _speed; } }
    public Sprite SpriteBall { get { return _spriteBall; } }
    public string Description { get { return _description; } }
    public int Price { get { return _price; } }
}
