using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Data", menuName = "Player Data")]

public class PlayerData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private float _health;
    [SerializeField] private float _currentHealth;
    [SerializeField] private float _ki;
    [SerializeField] private float _speed;

    public string Name { get { return _name; } set { _name = value; } }
    public float Health { get { return _health; } set { _health = value; } }
    public float CurrentHealth { get { return _currentHealth; } set { _currentHealth = value; } }
    public float Ki { get { return _ki; } set{ _ki = value; } }
    public float Speed { get { return _speed; } set { _speed = value; } }

}
