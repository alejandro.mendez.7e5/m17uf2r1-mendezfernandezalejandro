using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventary", menuName = "Inventory")]
public class Inventario : ScriptableObject
{
    //public HabilityData[] Hability = new HabilityData[5];
    public List<HabilityData> Hability;

    public void OutInventary()
    {
        Hability.Clear();
    }
}
