using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ManagerData", menuName = "ManagerData")]
public class ManagerData : ScriptableObject
{
    [SerializeField] private int _coins;
    [SerializeField] private int _scoreMax;
    [SerializeField] private int _scoreCurrent;

    public int Coins { get { return _coins; } set { _coins = value; } }
    public int Score { get { return _scoreMax; } set { _scoreMax = value; } }
    public int CurrentScore { get { return _scoreCurrent; } set { _scoreCurrent = value; } }
}
