using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIResults : MonoBehaviour
{
    [SerializeField] private ManagerData _controllerData;
    [SerializeField] private PlayerData _playerData;
    [SerializeField] private Text _pointsMax;
    [SerializeField] private Text _points;
    [SerializeField] private Text _enemiesDeath;
    [SerializeField] private Text _levelOk;
    [SerializeField] private Text _coins;
    [SerializeField] private GameObject _record;
    Data jsonData;
    // Start is called before the first frame update
    void Start()
    {
        SaveJson();
    }

    // Update is called once per frame
    void Update()
    {
        _pointsMax.GetComponent<Text>().text = _controllerData.Score.ToString();
        _points.GetComponent<Text>().text = _controllerData.CurrentScore.ToString();
        _coins.GetComponent<Text>().text = _controllerData.Coins.ToString();
        _enemiesDeath.GetComponent<Text>().text = GameManager.Instance.EnemiesDeath.ToString();
        _levelOk.GetComponent<Text>().text = GameManager.Instance.LevelCompleted.ToString();
        if (GameManager.Instance.record)
        {
            _record.SetActive(true);
        }
    }

    public void Play()
    {
        GameManager.Instance.ResetGame();
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void SaveJson()
    {
        string path = Application.persistentDataPath + "/data.json";
        string jsonString;

        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            jsonData = JsonUtility.FromJson<Data>(jsonString);

            if (jsonData == null) jsonData = new Data(0, 0, 0, 100, 100, 5);
        }
        else
        {
            File.Create(path);
            jsonData = new Data(0, 0, 0, 100, 100, 5);
        }
        jsonData.maxScore = _controllerData.Score;
        jsonData.lastScore = _controllerData.CurrentScore;
        jsonData.Coins = _controllerData.Coins;
        jsonData.Health = _playerData.Health;
        jsonData.Ki = _playerData.Ki;
        jsonData.Speed = _playerData.Speed;
        jsonString = JsonUtility.ToJson(jsonData);
        File.WriteAllText(path, jsonString);
    }
}
