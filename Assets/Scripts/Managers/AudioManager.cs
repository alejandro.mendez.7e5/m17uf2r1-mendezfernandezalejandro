using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager _audioManager;
    public AudioData audioListSO;
    public AudioMixerSnapshot AudioSnapGame;
    public AudioMixerSnapshot AudioSnapMenu;

    private void Awake()
    {
        if (_audioManager != null)
            Destroy(this.gameObject);
        _audioManager = this;

    }
    public void OnMenu()
    {
        AudioSnapMenu.TransitionTo(0.1f);
    }
    public void OnGame()
    {
        AudioSnapGame.TransitionTo(0.1f);
    }
}
