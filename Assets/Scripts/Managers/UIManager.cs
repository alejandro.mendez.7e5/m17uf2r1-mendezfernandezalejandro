using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private ManagerData _controllerData;
    [SerializeField] private PlayerData _playerData;
    [SerializeField] private Text _pointsMax;
    [SerializeField] private Text _coins;
    [SerializeField] private GameObject _shopPanel;
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private GameObject _optionPanel;
    [SerializeField] private GameObject _helpPanel;
    public Slider volumeMusic;
    public AudioMixer Music;
    public Slider volumeEffects;
    public AudioMixer Effects;
    public Toggle fullScreen;
    Data jsonData;

    void Start()
    {
        volumeMusic.onValueChanged.AddListener(ChangeVolumeMaster);
        volumeEffects.onValueChanged.AddListener(ChangeEffectsMaster);
        fullScreen.onValueChanged.AddListener(ChangeFullScreen);

        LoadJson();
    }

    void Update()
    {
        _pointsMax.GetComponent<Text>().text = "Max Score: "+_controllerData.Score.ToString();
        _coins.GetComponent<Text>().text = _controllerData.Coins.ToString();
    }

    public void Play()
    {
        GameManager.Instance.ResetGame();
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenShop()
    {
        _menuPanel.SetActive(false);
        _shopPanel.SetActive(true);
    }

    public void CloseShop()
    {
        _shopPanel.SetActive(false);
        _menuPanel.SetActive(true);
    }

    public void OpenOptions()
    {
        _menuPanel.SetActive(false);
        _optionPanel.SetActive(true);
    }

    public void CloseOptions()
    {
        _optionPanel.SetActive(false);
        _menuPanel.SetActive(true);
    }

    public void ChangeVolumeMaster(float v)
    {
        Music.SetFloat("VolMaster", v);
    }

    public void ChangeEffectsMaster(float v)
    {
        Effects.SetFloat("SfxMaster", v);
    }

    public void OnHelp()
    {
        _menuPanel.SetActive(false);
        _helpPanel.SetActive(true);
    }

    public void CLoseHelp()
    {
        _helpPanel.SetActive(false);
        _menuPanel.SetActive(true);
    }

    public void ChangeFullScreen(bool fullscreen)
    {
        Debug.Log(fullscreen);
        Screen.fullScreen = fullscreen;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void LoadJson()
    {
        string path = Application.persistentDataPath + "/data.json";
        Debug.Log(path);
        string jsonString;
        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            jsonData = JsonUtility.FromJson<Data>(jsonString);

            if (jsonData == null) jsonData = new Data(0, 0, 0, 100, 100, 5);
        }
        else
        {
            File.Create(path);
            jsonData = new Data(0, 0, 0, 100, 100, 5);
        }
        _controllerData.Score = jsonData.maxScore;
        _controllerData.CurrentScore = jsonData.lastScore;
        _controllerData.Coins = jsonData.Coins;
        _playerData.Health = jsonData.Health;
        _playerData.Ki = jsonData.Ki;
        _playerData.Speed = jsonData.Speed;
    }
}
