
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILevels : MonoBehaviour
{
    [SerializeField] private List<string> _level;
    private int _numScenes = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {      
        if (collision.gameObject.CompareTag("Player"))
        {
            _numScenes = Random.Range(0, _level.Count);
            //Debug.Log(_numScenes);
            if (!GameManager.Instance.nums.Contains(_numScenes))
            {
                GameManager.Instance.nums.Add(_numScenes);
                GameManager.Instance.Enemies = 20;
                GameManager.Instance.Level = _numScenes;
                GameManager.Instance.LevelCompleted += 1;
                SceneManager.LoadScene(_level[_numScenes]);

            }
            else
            {
                //Debug.Log(GameManager.Instance.Level + " V");
                GameManager.Instance.Victory();
            }
        }
    }
}
