using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIGame : MonoBehaviour
{
    [SerializeField] private ManagerData _controllerData;
    [SerializeField] private Text _points;
    [SerializeField] private Text _coins;
    [SerializeField] private GameObject _menuGamePanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _points.GetComponent<Text>().text = _controllerData.CurrentScore.ToString();
        _coins.GetComponent<Text>().text = _controllerData.Coins.ToString();
    }

    public void Menu()
    {
        GameManager.Instance.Level = 0;
        GameManager.Instance.NumKeys = 0;
        SceneManager.LoadScene("Menu");
    }

    public void OpenGameMenu()
    {
        _menuGamePanel.SetActive(true);
    }

    public void CloseGameMenu()
    {
        _menuGamePanel.SetActive(false);
    }
}
