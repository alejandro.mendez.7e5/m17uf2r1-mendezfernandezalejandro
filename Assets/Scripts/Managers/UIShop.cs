using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UIShop : MonoBehaviour
{
    [SerializeField] private ManagerData _contollerData;
    private HabilityData _hability;
    [SerializeField] private Text _coins;
    [SerializeField] private Text _nameHability;
    [SerializeField] private Text _desHability;
    [SerializeField] private Text _damageHability;
    [SerializeField] private Text _costHability;
    [SerializeField] private Text _priceHability;

    private static UIShop instance;
    
    public static UIShop Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UIShop>();
            }
            return instance;
        }
    }

    public void Update()
    {
        _coins.text = _contollerData.Coins.ToString();
    }

    public void UpdateUI(HabilityData bullet)
    {
        _nameHability.text = bullet.Name;
        _desHability.text = bullet.Description;
        _damageHability.text = bullet.Damage.ToString();
        _costHability.text = bullet.Cost.ToString();
        _priceHability.text = bullet.Price.ToString();
        _hability = bullet;
        
        
    }

    public HabilityData ReturnBullet()
    {
        return _hability;
    }
}
