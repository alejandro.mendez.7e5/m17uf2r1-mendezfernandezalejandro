using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data 
{
    public int lastScore, maxScore, Coins;
    public float Health, Ki, Speed;

    public Data(int last, int max, int coins, float health, float ki, float speed)
    {
        lastScore = last;
        maxScore = max;
        Coins = coins;
        Health = health;
        Ki = ki;
        Speed = speed;  
    }
}
