using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ManagerData _contollerData;
    public delegate void OnlyEnemy();
    public static event OnlyEnemy SpawnKey;

    public List<int> nums;

    public int Score;
    public int Enemies;
    public bool Key = false;
    public int NumKeys = 0;
    public int Level = -1;
    public bool record = false;
    public int EnemiesDeath;
    public int LevelCompleted;
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        Score = 0;
    }


    public void EventKey()
    {
        if (Enemies == 1)
        {
            Score += 100;
            _contollerData.Coins += 100;
            SpawnKey();
        }
    }

    public void Victory()
    {
        
        if (NumKeys >= 3)
        {
            SceneManager.LoadScene("Results");
            //Debug.Log("AAA" + NumKeys);
        }
    }

    public void Lose()
    {
        SceneManager.LoadScene("ResultsLose");
    }

    public void ResetGame()
    {
        EnemiesDeath = 0;
        LevelCompleted = 0;
        Enemies = 20;
        Score = 0;
        NumKeys = 0;
        Level = -1;
        record = false;
        nums.Clear();
        SceneManager.LoadScene("SampleScene");
    }
    public void CountEnemies()
    {
        Enemies--;
        EnemiesDeath++;
        EventKey();
    }

    public void CountScore()
    {
        Score++;
        if (_contollerData.Score < Score)
        {
            _contollerData.Score = Score;
            record = true;
        }
        _contollerData.CurrentScore = Score;
    }

    public void CountCoins()
    {
        _contollerData.Coins += 10;
    }
}
