using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Sprite _openDoor;
    //public Puerta
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (GameManager.Instance.Key)
            {
                OpenDoor();
                GameManager.Instance.NumKeys += 1;
                GameManager.Instance.Key = false;
            }
        }
    }
    private void OpenDoor()
    {
        Destroy(gameObject.GetComponent<BoxCollider2D>());
        gameObject.GetComponent<SpriteRenderer>().sprite = _openDoor;
    }
}
