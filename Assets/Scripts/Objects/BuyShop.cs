using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using UnityEngine;

public class BuyShop : MonoBehaviour
{
    public Inventario _inventary;
    public ManagerData _managerData;
    public PlayerData _playerData;
    Data jsonData;
    private HabilityData currentAbility;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetAbility(HabilityData bullet)
    {
        currentAbility = bullet;
    }

    public void BuyHabilty()
    {
        if (_managerData.Coins >= currentAbility.Price)
        {
            foreach (var hab in _inventary.Hability)
            {
                if (!_inventary.Hability.Find(x => x.Name.Contains(currentAbility.Name)))
                {
                    _inventary.Hability.Add(currentAbility);
                    _managerData.Coins -= currentAbility.Price;
                    break;
                }
            }
            SaveJson();
        }
    }

    public void BuyPowerUp()
    {
        if(_managerData.Coins >= 50 && _playerData.Health < 500)
        {
            _playerData.Health += 10;
            _playerData.Ki += 10;
            _managerData.Coins -= 50;
            SaveJson();
        }
    }

    public void BuySpeed()
    {
        if (_managerData.Coins >= 10 && _playerData.Speed < 10)
        {
            _playerData.Speed += 1;
            _managerData.Coins -= 10;
            SaveJson();
        }
    }

    public void SaveJson()
    {
        string path = Application.persistentDataPath + "/data.json";
        string jsonString;

        jsonString = File.ReadAllText(path);
        jsonData = JsonUtility.FromJson<Data>(jsonString);

        jsonData.maxScore = _managerData.Score;
        jsonData.lastScore = _managerData.CurrentScore;
        jsonData.Coins = _managerData.Coins;
        jsonData.Health = _playerData.Health;
        jsonData.Ki = _playerData.Ki;
        jsonData.Speed = _playerData.Speed;
        jsonString = JsonUtility.ToJson(jsonData);
        File.WriteAllText(path, jsonString);
    }

    private void OnEnable()
    {
        ShopHabilitys.setAbility += SetAbility;
    }
    private void OnDisable()
    {
        ShopHabilitys.setAbility -= SetAbility;
    }
}
