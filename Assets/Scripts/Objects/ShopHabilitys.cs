using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShopHabilitys : MonoBehaviour
{
    public static event Action<HabilityData> setAbility = delegate { };
    public HabilityData bullet;
    public GameObject ButtonShop;
    
    public void OnMouseDown()
    {
        UIShop.Instance.UpdateUI(bullet);
        bullet = UIShop.Instance.ReturnBullet();
        ButtonShop.SetActive(true);
        setAbility.Invoke(bullet);
        
    }
}
