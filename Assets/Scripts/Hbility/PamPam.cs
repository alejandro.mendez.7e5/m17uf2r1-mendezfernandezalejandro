using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


public class PamPam : MonoBehaviour, IDropeable
{
    [SerializeField] private Inventario _inventary;
    [SerializeField] private HabilityData[] _drop;
    [SerializeField] private PlayerData _playerData;
    private AudioSource _audioPam;

    private Image _energyKi;
    private float _ki;
    private float _timer = 0;
    private float _cooldown = 1;
    private int _numHability = 0;

    private Image _habilityImage;

    // Start is called before the first frame update
    void Start()
    {
        _ki = _playerData.Ki;
        _energyKi = GameObject.Find("Energy").GetComponent<Image>();
        _habilityImage = GameObject.Find("Hability").GetComponent<Image>();
        _audioPam = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0)) Pam();
        RecoveryKi();
        ChangeHability();
        _habilityImage.sprite = _inventary.Hability[_numHability].SpriteBall;
    }

    void Pam()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 0;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        _ki -= _inventary.Hability[_numHability].Cost; ;
        _energyKi.fillAmount = _ki / _playerData.Ki;

        if (_ki > 1)
        {
            _audioPam.clip = AudioManager._audioManager.audioListSO.AudioPamPlayer;
            _audioPam.Play();
            GameObject bullet = Instantiate(_inventary.Hability[_numHability].Ball, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.velocity = _inventary.Hability[_numHability].Speed * new Vector2(mousePos.x, mousePos.y).normalized;
        }
    }

    public void ChangeHability()
    {
        var mouseHability = Input.GetAxis("Mouse ScrollWheel");

        if(mouseHability > 0f)
        {
            _numHability++;
            if (_numHability >= _inventary.Hability.Count - 1)
            {
                _numHability = _inventary.Hability.Count - 1;
            }
            
        }
        else if (mouseHability < 0f)
        {
            _numHability--;
            if (_numHability <= 0)
            {
                _numHability = 0;
            }
            
        }
    }

    public void RecoveryKi()
    {
        _timer += Time.deltaTime;
        if( _timer > _cooldown )
        {
            _ki += 10;
            _ki = Mathf.Clamp( _ki, 0, _playerData.Ki);
            _energyKi.fillAmount = _ki / _playerData.Ki;
            _timer = 0;
        }
    }

    public void DropHability(string name)
    {
        int pos = 0;

        for (int i = 0; i < _drop.Length; i++)
        {
            if (_drop[i].Name == name)
            {
                pos = i;
            }
            
        }

        foreach (var hab in _inventary.Hability)
        {
            if (!_inventary.Hability.Find(x => x.Name.Contains(name)))
            {
                _inventary.Hability.Add(_drop[pos]);
                break;
            }
        }
    }
}
