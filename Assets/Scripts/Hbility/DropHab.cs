using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropHab : MonoBehaviour
{
    public HabilityData bullet;

    void Start()
    {

    }


    void Update()
    {
        Destroy(this.gameObject, 5f);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.TryGetComponent<IDropeable>(out IDropeable hability))
        {
            hability.DropHability(bullet.Name);
            Destroy(gameObject);
        }
    }
}
