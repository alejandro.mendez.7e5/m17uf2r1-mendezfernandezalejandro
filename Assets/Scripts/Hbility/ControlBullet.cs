using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBullet : MonoBehaviour
{
    // Start is called before the first frame update
    public HabilityData bullet;
    private int damage;
    private float stun;
    void Start()
    {
        damage = bullet.Damage;
        stun = bullet.Time;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(this.gameObject, 2f);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {


        if (collision.TryGetComponent<IDamage>(out IDamage hit))
        {
            hit.ApplyDamage(damage);
            Destroy(this.gameObject);
        }

        if (collision.TryGetComponent<Istuneable>(out Istuneable time))
        {
            time.ApplyStun(stun);
            Destroy(this.gameObject);
        }
    }
}
