using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Istuneable
{
    void ApplyStun(float time);
}
