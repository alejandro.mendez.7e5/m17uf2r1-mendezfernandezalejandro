using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : MonoBehaviour
{
    private List<ParticleCollisionEvent> _collisionEvents;
    private ParticleSystem _particles;
    protected Transform _objectFollow;
    [SerializeField] private HabilityData _fire;

    // Start is called before the first frame update
    void Start()
    {
        _particles = GetComponent<ParticleSystem>();
        _collisionEvents = new List<ParticleCollisionEvent>();
        _objectFollow = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        StartCoroutine(Fire());
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(_objectFollow);
    }

    public IEnumerator Fire()
    {
        yield return new WaitForSeconds(1f);
        _particles.Pause();
        _particles.Clear();
        yield return new WaitForSeconds(_fire.Time);
        _particles.Play();
        yield return Fire();
    }
    
    void OnParticleCollision(GameObject other)
    {
        //Debug.Log("DA�OOO");
        int numCollision = _particles.GetCollisionEvents(other, _collisionEvents);

        if(other.TryGetComponent<IDamage>(out IDamage hit))
        {
            hit.ApplyDamage(_fire.Damage * numCollision);
        }
    }
    /*
    private void OnParticleTrigger()
    {
        Debug.Log("DA�OOO");
    }*/
}
