using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoblinController : EnemyController, IDamage, Istuneable
{
    // Start is called before the first frame update
    void Start()
    {
        _objectFollow = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _sr = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();

        _hp = _enemyData.Health;
        _speed = _enemyData.Speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_stunMove)
        {
            Move();
        }
    }

    protected override void Move()
    {
        if (_objectFollow == null) return;
        var pos = transform.position.x;
        transform.position = Vector2.MoveTowards(transform.position, _objectFollow.transform.position, _speed * Time.deltaTime);
        if (transform.position.x < pos) _sr.flipX = true;
        else _sr.flipX = false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject.GetComponent<Collider2D>());
            _audioDeath.clip = AudioManager._audioManager.audioListSO.AudioDeathEnemy;
            _audioDeath.Play();
            _objectFollow = null;
            _anim.SetBool("Death", true);
            Destroy(this.gameObject, 1f);
        }
    }

    protected override void DestroyObject()
    {
        Destroy(this.gameObject.GetComponent<Collider2D>());
        _audioDeath.clip = AudioManager._audioManager.audioListSO.AudioDeathEnemy;
        _audioDeath.Play();
        _objectFollow = null;
        _anim.SetBool("Death", true);
        Destroy(this.gameObject, 1f);
        SpawnObject();
        GameManager.Instance.CountScore();
        GameManager.Instance.CountEnemies();
    }

    private void OnEnable()
    {
        GameManager.SpawnKey += SpawnKey;
    }
    private void OnDisable()
    {
        GameManager.SpawnKey -= SpawnKey;
    }
}
