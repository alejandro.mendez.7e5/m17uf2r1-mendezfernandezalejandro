using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemies : MonoBehaviour
{
    public GameObject[] _enemies;
    private float _timer = 0;
    public float _cooldown = 0;
    public float maxX, maxY, minX, minY;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (GameManager.Instance.Level == -1)
        {
            float posx = Random.RandomRange(minX, maxX);
            float posy = Random.RandomRange(minY, maxY);
            
            if (_timer > _cooldown && GameManager.Instance.Enemies >= 1)
            {
                if (GameManager.Instance.Enemies > 1)
                {
                    Instantiate(_enemies[1], new Vector3(posx, posy, 0), Quaternion.identity);
                    Instantiate(_enemies[0], new Vector3(posx, posy, 0), Quaternion.identity);
                    _timer = 0;
                }

            }
        }

        if (GameManager.Instance.Level == 0)
        {
            float posx = Random.RandomRange(minX, maxX);
            float posy = Random.RandomRange(minY, maxY);

            if (_timer > _cooldown && GameManager.Instance.Enemies >= 1)
            {
                if (GameManager.Instance.Enemies > 1)
                {
                    Instantiate(_enemies[2], new Vector3(posx, posy, 0), Quaternion.identity);
                    _timer = 0;
                }

            }
        }

        if (GameManager.Instance.Level == 1)
        {
            float posx = Random.RandomRange(minX, maxX);
            float posy = Random.RandomRange(minY, maxY);

            if (_timer > _cooldown && GameManager.Instance.Enemies >= 1)
            {
                if (GameManager.Instance.Enemies > 1)
                {
                    Instantiate(_enemies[3], new Vector3(posx, posy, 0), Quaternion.identity);
                    _timer = 0;
                }

            }
        }

        if (GameManager.Instance.Level == 2)
        {
            float posx = Random.RandomRange(minX, maxX);
            float posy = Random.RandomRange(minY, maxY);

            if (_timer > _cooldown && GameManager.Instance.Enemies >= 1)
            {
                if (GameManager.Instance.Enemies > 1)
                {
                    Instantiate(_enemies[4], new Vector3(posx, posy, 0), Quaternion.identity);
                    _timer = 0;
                }

            }
        }
    }
}
