using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : CharacterController, Istuneable
{
    protected bool _stunMove = true;

    [SerializeField] protected float spawn;

    [SerializeField] protected EnemyData _enemyData;

    protected Transform _objectFollow = null;
    protected virtual void SpawnObject() 
    {
        var probability = Random.RandomRange(0, 100);
        var item = Random.RandomRange(0, _enemyData._loot.Length);

        if (probability > spawn) Instantiate(_enemyData._loot[item], new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
    }
    protected virtual void SpawnKey() 
    {
        Instantiate(_enemyData.Key, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
    }

    void Istuneable.ApplyStun(float time)
    {
        StartCoroutine(Stun(time));
    }

    IEnumerator Stun(float time)
    {
        _stunMove = false;
        yield return new WaitForSeconds(time);
        _stunMove = true;
    }
}
