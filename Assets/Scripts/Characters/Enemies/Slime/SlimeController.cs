using System.Collections;
using System.Collections.Generic;
using UnityEditor.Tilemaps;
using UnityEngine;

public class SlimeController : EnemyController, IDamage
{
   private float _timer = 0;
    private float _cooldown = 2;

    private bool _death = false;

    // Start is called before the first frame update
    void Start()
    {
        _objectFollow = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _sr = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();

        _hp = _enemyData.Health;
        _speed = _enemyData.Speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_stunMove)
        {
            Move();
            Shoot();
        } 
    }

    public void Shoot()
    {
        _timer += Time.deltaTime;
        if(_timer > _cooldown && !_death)
        {
            GameObject bullet = Instantiate(_enemyData.Bullet, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            bullet.GetComponent<Rigidbody2D>().velocity = _enemyData.VelovityBullet * new Vector2(_objectFollow.transform.position.x - transform.position.x, _objectFollow.transform.position.y - transform.position.y).normalized;
            _timer = 0;
        }
    }

    protected override void Move()
    {
        if (_objectFollow == null) return;
        var pos = transform.position.x;
        transform.position = Vector2.MoveTowards(transform.position, _objectFollow.transform.position, _speed * Time.deltaTime);
        if (transform.position.x < pos) _sr.flipX = true;
        else _sr.flipX = false;
    }

    protected override void DestroyObject()
    {
        Destroy(this.gameObject.GetComponent<Collider2D>());
        _audioDeath.clip = AudioManager._audioManager.audioListSO.AudioDeathEnemy;
        _audioDeath.Play();
        _objectFollow = null;
        _anim.SetBool("Death", true);
        Destroy(this.gameObject, 1f);
        _death = true;
        SpawnObject();
        GameManager.Instance.CountScore();
        GameManager.Instance.CountEnemies();
    }

    private void OnEnable()
    {
        GameManager.SpawnKey += SpawnKey;
    }
    private void OnDisable()
    {
        GameManager.SpawnKey -= SpawnKey;
    }
}
