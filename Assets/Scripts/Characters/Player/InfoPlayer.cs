using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class InfoPlayer : CharacterController
{
    [SerializeField] private PlayerData _playerData;
    [SerializeField] private Sprite[] _sprites;

    private Vector2 _move;

    private bool canDash = true;
    private bool isDashing;
    private float dashingPower = 20f;
    private float dashingTime = 0.2f;
    private float dashingCooldown = 3f;

    // Start is called before the first frame update
    void Start()
    {
        _hp = _playerData.Health;
        _speed = _playerData.Speed;
        _img = GameObject.Find("Health").GetComponent<Image>();
        _anim = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        if(GameManager.Instance.Level != -1)
        {
            CurrentHealth();
            DamageHP();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!isDashing) Move();

        if (Input.GetKeyDown(KeyCode.Space) && canDash)
        {
            _audioTP.clip = AudioManager._audioManager.audioListSO.AudioTPlayer;
            _audioTP.Play();
            StartCoroutine(Dash());
        }  
    }

    protected override void Move()
    {
        _move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        _anim.SetBool("Run", _move.x != 0.0f || _move.y != 0.0f);
        _move = _move.normalized;
        _rb.velocity = _speed * _move;
        _anim.SetFloat("BlendX", _move.x);
        _anim.SetFloat("BlendY", _move.y);
    }
    
    public void CurrentHealth()
    {
        _hp = _playerData.CurrentHealth;
    }
    public void DamageHP()
    {
        _img.fillAmount = _hp/ _playerData.Health;
        _playerData.CurrentHealth = _hp;
    }

    public void RecoverHP()
    {
        _hp += 10;
        _hp = Mathf.Clamp(_hp, 0, _playerData.Health);
        _img.fillAmount = _hp / _playerData.Health;
        _playerData.CurrentHealth = _hp;
    }

    public IEnumerator Dash()
    {
        canDash = false;
        isDashing = true;
        float originalGravity = _rb.gravityScale;
        _rb.gravityScale = 0f;
        _rb.velocity = _move * dashingPower;
        yield return new WaitForSeconds(dashingTime);
        _rb.gravityScale = originalGravity;
        isDashing = false;
        yield return new WaitForSeconds(dashingCooldown);
        canDash = true;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            _hp -= 10;
            DamageHP();
            if (_hp <= 0)
            {
                DestroyObject();
            }
        }

        if (collision.gameObject.CompareTag("Heart"))
        {
            Destroy(collision.gameObject);
            if (_hp != _playerData.Health)
            {
                RecoverHP();
            }
        }

        if (collision.gameObject.CompareTag("Key"))
        {
            GameManager.Instance.Key = true;
            Destroy(collision.gameObject);
        }
    }

    protected override void DestroyObject()
    {
        _anim.SetBool("Death", true);
        _audioDeath.clip = AudioManager._audioManager.audioListSO.AudioDeathPlayer;
        _audioDeath.Play();
        StartCoroutine("Stop");       
    }

    public override void ApplyDamage(int damageTaken)
    {
        _hp -= damageTaken;
        DamageHP();
        if (_hp <= 0)
        {
            DestroyObject();
        }
    }

    IEnumerator Stop()
    {
        yield return new WaitForSeconds(0.3f);
        GameManager.Instance.Lose();
    }
}
