using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour, IDamage
{
    protected float _hp;
    protected float _speed;
    protected SpriteRenderer _sr;
    protected Animator _anim;
    protected Image _img;
    protected Rigidbody2D _rb;
    protected AudioSource _audioDeath;
    protected AudioSource _audioTP;
    private void Awake()
    {
        _audioDeath = GetComponent<AudioSource>();        _audioTP = GetComponent<AudioSource>();
    }
    protected virtual void Move() { }
    protected virtual void DestroyObject() { }

    public virtual void ApplyDamage(int damageTaken)
    {
        _hp -= damageTaken;

        if (_hp <= 0)
        {
            DestroyObject();
        }
    }
}
